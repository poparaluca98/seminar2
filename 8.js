let map=(a,t)=>{
	let result=[]
	for(let e of a){
		result.push(t(e))
	}
	return result
}
let sampleArray=[1,2,3,4,5]
console.log(map(sampleArray,(x)=>x*x))
console.log(sampleArray.map((x)=>x*x))