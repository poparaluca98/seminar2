let fib = (n) =>{
	switch(n){
		case 0:
		case 1:
		return 1
		default:
		return fib(n-1)+fib(n-2)
	}
}
//console.log(fib(6))
if (process.argv.length<3){
	console.log('usage: node 2.js <order>')
}
else
{
	console.log(fib(parseInt(process.argv[2])))
}