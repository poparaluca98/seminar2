let format=(s, formatSpec)=>
{
	let modified=s
	for(let i=0;i<formatSpec.length;i++){
		if(s.indexOf('{'+i+'}')!==-1){
			modified=modified.replace('{'+i+'}', formatSpec[i])
		}
	}
	return modified
}
console.log(format(" i am a {0} string and i am {1} ",["little","formatted"]))
